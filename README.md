# Collection of several Moduls Library

A pure-Python library for machine learning workflows.
A pure-Python library for helper functions.

## Installation

Install the library into the active virtual environment:

```shell
(venv) $ python -m pip install .
```

Activate another environment
e.g. call c:\Users\nerdh\PycharmProjects\machine-learning\.venv\Scripts\activate.bat


### Packaging

Build and package a stand-alone tic-tac-toe library for distribution:

```shell
$ python -m pip wheel .
```
