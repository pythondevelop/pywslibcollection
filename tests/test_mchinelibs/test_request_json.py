import logging

import pytest

from analysedata_picker.api_request_json import gen_records_fromdataset

pytestmark = [pytest.mark.machinelearning]


def test_gen_records_fromdataset():
    api_query_s = 'https://datasource.kapsarc.org/api/records/1.0/search/?dataset=solar-consumption-in-mtoe&facet=year&facet=million_tonnes_oil_equivalent&refine.year=2017'
    records = gen_records_fromdataset(api_query_s, logging.getLogger())
    assert records


"""
rows = list(gen_fields_fromdataset(records))
csv_table = df.to_csv()
print(df)
print(csv_table)
"""
