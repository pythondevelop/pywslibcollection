import logging
from pathlib import Path
import pytest

from analysedata_picker import DataManager, DatasetInfo, CsvParsingInfo

pytestmark = [pytest.mark.machinelearning]


def test_suite(request):
    assert True


def test_save_data_from_url():
    dataset = DatasetInfo.get_dataset('housing_california')[0]
    logging.info(dataset.url)
    filepath = DataManager.save_data_from_url(dataset.url)
    logging.info(filepath)
    assert filepath


def test_getdataset():
    dataset_a = list(filter(lambda c: c.id == 'iris', DatasetInfo.get_ml_dataset_info()))[0]
    dataset_b = DatasetInfo.get_dataset('iris')[0]
    logging.info(dataset_a)
    logging.info(dataset_b)
    assert dataset_a == dataset_b


def test_create_panda_from_zipped():
    dataset = DatasetInfo.get_dataset('housing_california')[0]
    logging.info(dataset.url)
    ds = DataManager.create_panda_from_zipped(dataset.url, path=('res', 'tmp-dataset'))
    logging.info(ds.info)
    assert ds.empty == False


def test_dataframe_from_csv():
    dataset = list(filter(lambda c: c.id == 'iris', DatasetInfo.get_ml_dataset_info()))[0]
    DataManager.save_data_from_url(dataset.url)
    df = DataManager.create_panda_fromcsv(CsvParsingInfo(path=Path(dataset.save_path) / "iris.data", seperator=','))
    assert df.empty == False


def test_dataframe_from_csv_oecd():
    dataset = list(filter(lambda c: c.id == 'lifesat', DatasetInfo.get_ml_dataset_info()))[0]
    DataManager.save_data_from_url(dataset.url)
    df = DataManager.create_panda_fromcsv(
        CsvParsingInfo(path=Path(dataset.save_path) / "oecd_bli_2015.csv", seperator=','))
    assert df.empty == False


def test_get_ml_dataset_info_ids():
    logging.info(DatasetInfo().get_ml_dataset_info_ids())
    assert True
