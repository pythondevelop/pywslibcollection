import logging
from pathlib import Path
import numpy as np
import pytest

from analysedata_picker import DatasetInfo
from analysedata_picker import DataManager, CsvParsingInfo
from neural_network import Perceptron as pc

pytestmark = [pytest.mark.machinelearning]


def test_predective_smoketest():
    training = pc(0.05, 1000)

    input_data = [[1, 2, 3], [4, 5, 6]]
    y = [5, 10]
    training.fit(np.asarray(input_data), y)
    pred = training.predict(np.asarray(input_data))
    logging.info(f'Prediction Result : {pred}')
    assert True


def test_predective_iris():
    dataset = list(filter(lambda c: c.id == 'iris', DatasetInfo.get_ml_dataset_info()))[0]
    DataManager.save_data_from_url(dataset.url)
    df = DataManager.create_panda_fromcsv(CsvParsingInfo(path=Path(dataset.save_path) / "iris.data", seperator=','))
    logging.info(df.info())
    logging.info(df.iloc[:50, [0, 1, 2]].values)
    iris_matrix = df.iloc[:150, [0, 2]].values
    Y = df.iloc[:150, [4]]
    logging.info(iris_matrix[:-5, 0])

    clasifier = pc()
    clasifier.fit(iris_matrix, Y)
    clasifier.predict(iris_matrix)
