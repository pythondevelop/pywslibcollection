import logging

import math
import time

import pytest

from common_snippets import Stopwatch, measuretime



@pytest.fixture
def measured_function():
    def _function():
        for i in range(1, 10000000):
            result = math.sin(math.log10(i))
            if i % 1000000 == 0:
                logging.info(f'math.sin(math.log10(i)) result == {result}')
        return True
    return _function

def loop(ADD_SCARR_TIMEOUT_S=10):
    stopwatch = Stopwatch()
    stopwatch.start()
    while True:
        time.sleep(0.5)
        print(f"runtime {stopwatch.elapsed_Time}")
        if stopwatch.elapsed_Time > ADD_SCARR_TIMEOUT_S:
            break


def test_stopwatch_control_measure(measured_function):
    watch = Stopwatch(text=f'internal elapsed time {{:0.4f}} s', logger=logging.getLogger())
    watch.start()
    verdict = measured_function()
    logging.info(watch.elapsed_Time)
    watch.stop()
    assert True


def test_stopwatch_control_stop():
    watch = Stopwatch(logger=logging.getLogger('Island'))
    watch.start()
    time.sleep(5)
    watch.stop()
    logging.info(f'stopped Time -- {watch.stoptime}')
    assert watch.elapsed_Time is not None


def test_stopwatch_accumulated():
    watch = Stopwatch(name='accumulated', logger=logging.getLogger('Island'))
    watch.start()
    time.sleep(5)
    watch.stop()
    watch = Stopwatch(name='accumulated', logger=logging.getLogger('Island'))
    watch.start()
    time.sleep(5)
    watch.stop()
    logging.info(f'accumulated stopped Time -- {watch.timers["accumulated"]}')
    assert watch.timers['accumulated'] is not None
    assert int(watch.timers['accumulated']) == 10


def test_stopwatch_context_mgr(measured_function):
    with Stopwatch(name='Moontime', text=f'Running the Moon in {{:0.6f}} seconds', logger=logging) as clock:
        verdict = measured_function()
        logging.info(f'Running the Moon in {clock.elapsed_Time:0.6f} seconds')

    assert clock

@measuretime
def test_stopwatch_decorator_simple(measured_function):
    verdict = measured_function()
    assert verdict

def test_stopwatch_decorator():
    @Stopwatch(text='jetzt auch als decorator in {:.2f} seconds', logger=logging.getLogger('Island'))
    def rechnen():
        for i in range(1, 10000000):
            result = math.sin(math.log10(i))
            if i % 1000000 == 0:
                logging.info(f'math.sin(math.log10(i)) result == {result}')
        return True

    verdict = rechnen()
    verdict = rechnen()

    assert verdict



# @pytest.mark.skip(reason="only to simulate errors")
@pytest.mark.xfail(reason="only to simulate errors")
def test_stopwatch_control_error():
    watch = Stopwatch(text=f'internal elapsed time {{:0.4f}} s')
    watch.start()
    for i in range(1, 10000000):
        math.sin(math.log10(i))
    logging.info(watch.elapsed_Time)
    watch.stop()
    assert False
