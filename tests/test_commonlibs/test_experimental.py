import logging
from typing import Tuple

import pytest

pytestmark = [pytest.mark.experimental]

"""
ideal

test_params = [
    pytest.param('TargetPowerTPCMapping.ABS', id=du_info + ', TargetPowerTPCMapping.ABS'),
   pytest.param('TargetPowerTPCMapping.ACC', id=du_info + ', TargetPowerTPCMapping.ACC'),
 ]

Testline Info like ue_id should part of the test_name ...
with id 

gelernt. man kriegt das fixture nicht in die id function. Fixture in Fixture in test_func geht alles

LÖSUNG: 
@pytest.hookimpl(tryfirst=True)
def pytest_make_parametrize_id(config, val):

"""
# jetzt
# pytest.fixture not works with pytest.param, correct?
test_params = [
    pytest.param('TargetPowerTPCMapping.ABS', marks=pytest.mark.HiSi_Balong5000, id='HiSi_Balong5000'),
    pytest.param('TargetPowerTPCMapping.ACC', marks=pytest.mark.HiSi_Balong5000, id='HiSi_Balong5000'),
    pytest.param('TargetPowerTPCMapping.ACC', marks=pytest.mark.LG_V50, id='LG_V50'),
    pytest.param('TargetPowerTPCMapping.ABS', marks=pytest.mark.LG_V50, id='LG_V50'),
    pytest.param('TargetPowerTPCMapping.ACC', marks=pytest.mark.QC_MTP_SM8250, id='QC_MTP_SM8250'),
    pytest.param('TargetPowerTPCMapping.ABS', marks=pytest.mark.QC_MTP_SM8250, id='QC_MTP_SM8250'),
]


#@pytest.mark.parametrize("mapping", test_params)
#oder
@pytest.fixture(params=test_params)
def _param_fixture(request):
    return request.param
# oder Generator


def params_generator():
    for element in [
        pytest.param('TargetPowerTPCMapping.ABS', marks=pytest.mark.HiSi_Balong5000),
        pytest.param('TargetPowerTPCMapping.ABS', marks=pytest.mark.HiSi_Balong5000)
    ]:
        yield element

@pytest.fixture()
def params_generator_fix(get_cli_custom_option):
    ue_id = get_cli_custom_option if get_cli_custom_option else ''

    for element in [
        pytest.param('TargetPowerTPCMapping.ABS', marks=pytest.mark.HiSi_Balong5000, id=ue_id),
        ('TargetPowerTPCMapping.ACC')
    ]:
        yield element


@pytest.mark.parametrize("mapping", params_generator())
def test_nr_nsa_ul_powercontrol_mapping_system(mapping, get_cli_custom_option):
    """Run on target system."""
    logging.info(get_cli_custom_option)
    assert True
