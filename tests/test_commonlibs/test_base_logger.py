import logging
from typing import Optional

import sys


def test_logging_color_sets():
    assert True


def test_create_new_logger():
    from custom_logger.base_logger import LoggerFactory

    loggers = LoggerFactory()
    test_logger = loggers.register_new_logger("test_logger")
    test_logger.info("registered logger works")
    test_logger = loggers.register_new_logger("test_logger_second")
    test_logger.info("registered logger works")

    assert loggers.loggers["test_logger"] and loggers.loggers["test_logger_second"]


def test_root_simple_logger():
    from custom_logger.base_logger import (
        LoggerFactory,
        LoggingColorBasic,
    )

    LoggerFactory(color=LoggingColorBasic.RED).root_simple_logger.log(
        logging.WARNING, "WARNING Message"
    )
    assert True


def dummy_logger():
    dummy = logging.getLogger("dummy")
    dummy.setLevel(logging.CRITICAL)
    return dummy


## Optional Logger as function arg
# EXPIREMENT
# logging ausschalten, mal darüber nachdenken ob das sinn macht
# striktere Trennung von User output und logging output für pytest
def test_funcional_logger_optional():
    # dummy logger
    def gen_records_fromdataset(
        api_qery: str, logger: Optional[logging.Logger] = None
    ):
        log = logger or logging.getLogger("dummy")  # without configuring dummy before.

    # oder Level auf höchsten Level
    logging.basicConfig()  # easily setup a StreamHandler output
    logging.getLogger("realm1").setLevel(logging.ERROR)


print(f"\n".join(sys.path))
