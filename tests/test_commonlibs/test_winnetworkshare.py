import os
import sys
import logging

import subprocess

from remote import InstrumentNetworkShareWindows


def test_connect_testsystemdrive():
    ("Python {:s} on {:s}\n".format(sys.version, sys.platform))
    if not sys.platform.startswith("win"):
        logging.warning("platform is not windows")
        assert False
    InstrumentNetworkShareWindows().connect_testsystemdrive(
        "CMW50050-167849", "c\\ProgramData\\Rohde-Schwarz\\DUT\\logs"
    )
    subprocess.check_call(["net", "use"])
    subprocess.check_call(
        ["powershell", "Get-CimInstance", "-Class", "Win32_NetworkConnection"]
    )
    logging.info(
        *os.listdir("\\\\CMW50050-167849\\c\\ProgramData\\Rohde-Schwarz\\DUT\\logs")
    )
    assert True


def test_connect_testsystemdrive_create():
    connnector = InstrumentNetworkShareWindows()
    assert connnector


def test_connect_testsystemdrive_defaults():
    connnector = InstrumentNetworkShareWindows()
    assert connnector.SHARE_PWD == "instrument"


def test_list_files_localhost():
    logging.info(InstrumentNetworkShareWindows.build_sharefullname())
    logging.info(list(InstrumentNetworkShareWindows.list_files()))
    assert True

def test_list_files():
    logging.info(InstrumentNetworkShareWindows.build_sharefullname(host_name='CMW50050-167849',share_name=r'c\ProgramData\Rohde-Schwarz\DUT\logs'))
    logging.info(list(InstrumentNetworkShareWindows.list_files(host_name='CMW50050-167849',share_name=r'c\ProgramData\Rohde-Schwarz\DUT\logs')))
    assert True

