import logging


def test_table_output():
    from ascii_outputs.tabulates import tabulate

    logging.info("")
    logging.info(
        f'simple table \n {tabulate([[1, 2.34], [-56, "8.999"], ["2", "10001"]])}'
    )
    assert True
