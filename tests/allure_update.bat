@ECHO OFF

ECHO copy history
if exist .\allure-report\history xcopy /Y .\allure-report\history .\allure-results\history\


ECHO generate reports

allure generate .\allure-results --clean
