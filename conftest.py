import logging
import  os
import sys

import pytest
"""
https://docs.pytest.org/en/stable/pythonpath.html
Prinzip Test modules / but not conftest.py files inside packages¶

test fixture
https://hackebrot.github.io/pytest-tricks/param_id_func/
https://github.com/okken/talks/blob/master/2020/pycon_2020/presentation.md
https://docs.pytest.org/en/stable/fixture.html#fixture
"""



def pytest_addoption(parser):
    """Adds --test_param, --dutconfig to pytest cli options"""
    parser.addoption(
        "--test_param", action="store", default=False, help="specifies an rstp file to use."
    )
    parser.addoption(
        "--enable", action="store_true", default=False, help="flag - generel disable of rsxp usage for "
    )
def get_module_att():
    return f'# {__name__} ~ {__file__} \n'


cli_args = []

@pytest.hookimpl(tryfirst=True)
def pytest_sessionstart(session):
    line = len(get_module_att())
    line = int(line/2)  *'~#' +'\n'
    paths = '\n# '.join(p for p in sys.path[:] if  'Python37' and '.venv' not in p)
    logging.info(f'\n'
      f'#{line}'
      f'# .FILE INFO HEADER \n'
      f'{get_module_att()}'
      f'{paths}\n'
      f'# CWP : {os.getcwd()}\n'                
      f'#{line}'
      )
    global cli_args
    cli_args = [ session.config.getoption("--test_param"),
    session.config.getoption("--enable")]


@pytest.fixture(autouse=True)
def system_test_execution_fixture(request):
    logging.info(f'Call of {system_test_execution_fixture.__name__}')

@pytest.fixture()
def get_global_settings(pytestconfig):
    logging.info(f'Call of {get_global_settings.__name__}')
    return 'id 42'


@pytest.fixture()
def get_cli_custom_option():
    logging.info(f'Call of {get_cli_custom_option.__name__}')
    return cli_args

@pytest.hookimpl(tryfirst=True)
def pytest_make_parametrize_id(config, val):
    logging.info(f'Call of {pytest_make_parametrize_id.__name__}')

    return repr(cli_args)



