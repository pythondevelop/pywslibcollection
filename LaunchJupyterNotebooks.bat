@ECHO OFF

ECHO call "..\.venv\Scripts\activate.bat"

ECHO "Set JUPYTER_CONFIG_DIR"
SET JUPYTER_CONFIG_DIR=.jupyter
SET JUPYTER_PATH=.jupyter

ECHO jupyter nbextension enable "RS_notebook_extension/RS_nb_extension"

jupyter notebook "jupyter"

