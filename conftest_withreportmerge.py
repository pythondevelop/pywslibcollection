import logging
import os
import sys

import pytest

#C:\Users\Public\Documents\Rohde-Schwarz\XLAPI\script-cwsp\1c-sisu\SystemTest\tests\system\test_system_support
#C:\Users\Public\Documents\Rohde-Schwarz\XLAPI\script-cwsp\1c-sisu\SystemTest\tests\unit\conftest.py

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../system/')))

# sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../../SystemTest/')))

@pytest.hookimpl(tryfirst=True)
def pytest_sessionfinish(session, exitstatus):
    from pathlib import Path
    from  datetime import datetime
    import time
    """ called after whole test run finished, right before returning the exit status to the system.

    Args:
        _pytest.main.Session session: the pytest session object
        int exitstatus: the status which pytest will return to the system
    """
    logging.info('pytest_sessionfinish started')

    # TODO: merge report.xml from multiple pytest runs
    if(Path('report/report.xml').exists()):
        logging.info('report/report.xml exist')
        x = session
        time = datetime.now().strftime("%H_%M_%S")
        new_path = f'report/report_{time}.xml'
        Path('report/report.xml').rename(Path(new_path))
    #        with open('report.xml', 'r', encoding='utf-8') as infile:
    #            content = infile.read()
    # append from session start read content

    # restore_dut_control_config()

    logging.info('pytest_sessionfinish finished')

def filter_report_files(pattern='report/report*.xml'):
    from pathlib import Path
    path_ = Path('.')
    # lookup in current dir
    tuple(path_.glob(pattern))
    # lookup recursive
    return list(path_.rglob(pattern))

@pytest.hookimpl(tryfirst=True)
def pytest_unconfigure(config):
    from pathlib import Path
    report_root = 'report/'
    logging.info('merge reports ')
    from junitparser import JUnitXml

    reports = filter_report_files()
    logging.info(*reports)
    xml1 = None

    try:
        if Path(report_root+ 'report.xml') in reports:
            xml1 = JUnitXml.fromfile(report_root+ 'report.xml')
            reports.remove(Path(report_root+ 'report.xml'))

        for report in reports:
            xml2 = JUnitXml.fromfile(report)
            xml1 = xml1 + xml2
        xml1.write()
    except:
        logging.info(*reports)


#
#    xml2 = JUnitXml.fromfile('/path/to/junit2.xml')

#    newxml = xml1 + xml2
    # Alternatively, merge in place
#    xml1 += xml2