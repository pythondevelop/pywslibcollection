"""
Data Container for save Internet location of example datas

    csv -> use panda.read_csv

    module which helps: urllib3, beutifulSoap4, json, panda, os

"""

import dataclasses
import json
import logging
import string
from dataclasses import dataclass
from enum import Enum
from typing import List

import yaml


class DatasetFormat(str, Enum):
    CSV = 'CSV'
    JSON = 'JSON'
    XML = 'XML'
    SQL = 'SQL'
    YAML = 'YAML'
    TGZ = 'TGZ'


@dataclass
class CsvParsingInfo():
    path: string
    seperator: string
    header: int = 0
    is_local_file: bool = True


@dataclass
class OpenDatasetUrl():
    url: string
    dataformat: DatasetFormat
    save_path: string
    id: string
    description: string = 'training set'
    is_tgz: bool = False
    api_query: str = False


"""# TODO Daten von API holen oder statische link in ein JSON File ablegen, ist besser editierbar
    # more data list on page 36 in handson
    # https://image-net.org/
          # https://datasource.kapsarc.org/explore/dataset/solar-consumption-in-mtoe/download/
        # ?format=csv&timezone=Europe/Berlin&lang=en&use_labels_for_header=true&csv_separator=%3B

     
    """


class DatasetInfo():
    # Transform this to a class factory approach
    _dataset_info = [
        OpenDatasetUrl(id='lifesat',
                       url='https://raw.githubusercontent.com/ageron/handson-ml/master/datasets/lifesat/oecd_bli_2015.csv',
                       dataformat=DatasetFormat.CSV,
                       save_path='res/tmp-dataset')
        , OpenDatasetUrl(id='solar_eu',
                         url='https://datasource.kapsarc.org/explore/dataset/solar-consumption-in-mtoe/download/?format=csv&disjunctive.million_tonnes_oil_equivalent=true&refine.year=2018&timezone=Europe/Berlin&lang=en&use_labels_for_header=true&csv_separator=%3B',
                         dataformat=DatasetFormat.CSV,
                         save_path='res/tmp-dataset',
                         description='solar consumption in million_tonnes_oil_equivalent')
        , OpenDatasetUrl(id='iris',
                         url='https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data',
                         dataformat=DatasetFormat.CSV,
                         save_path='res/tmp-dataset',
                         description='solar consumption in million_tonnes_oil_equivalent,https://datasource.kapsarc.org/explore/dataset/solar-consumption-in-mtoe')
        , OpenDatasetUrl(id='housing_california',
                         url='https://raw.githubusercontent.com/ageron/handson-ml2/master/datasets/housing/housing.tgz',
                         dataformat=DatasetFormat.TGZ,
                         save_path='res/tmp-dataset',
                         description='housing value archive from california')
        , OpenDatasetUrl(id='wine_red',
                         url='https://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/winequality-red.csv',
                         dataformat=DatasetFormat.CSV,
                         save_path='res/tmp-dataset',
                         description='wine quality ')

    ]
# TODO: NEXT for serialize to yaml

    def export_dataset_descriptor(self):
        # Write YAML file
        with open('open-dataset.yaml', 'w+', encoding='utf8') as outfile:
            try:
                yaml.dump([{element.id: dataclasses.asdict(element)} for element in self._dataset_info],
                          outfile,
                          indent=4,
                          default_flow_style=False,
                          allow_unicode=True)
                logging.info('yaml dump finished')
            except Exception as ex:
                logging.warning(f'yaml export failed : {ex.args}')
        with open('open-datasets-descriptor.json', 'w+', encoding='utf8') as jstream:
            try:
                gen = [{el.id: dataclasses.asdict(el)} for el in self._dataset_info]
                json.dump(gen,
                          jstream,
                          indent=4,
                          separators=(", ", ": ")
                          )
                logging.info('json dump finished')
            except Exception as ex:
                logging.warning(f'json export failed : {ex.args}')
    @classmethod
    def get_ml_dataset_info(cls) -> List:
        return cls._dataset_info

    @classmethod
    def get_dataset(cls, id: string) -> List[OpenDatasetUrl]:
        return list(filter(lambda c: c.id == id, cls._dataset_info))

    def get_ml_dataset_info_ids(self) -> List:
        dataset_keys = [element.id for element in self._dataset_info]
        return dataset_keys
