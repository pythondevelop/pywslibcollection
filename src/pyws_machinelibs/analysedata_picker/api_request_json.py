import json
from logging import Logger
from typing import Optional

import pandas
import urllib3

from custom_logger import logger_wrapper

"""
Test page of REST-API https://jsonplaceholder.typicode.com/
Request on https://datasource.kapsarc.org/api
with conversion to panda table data from json 
extract table from json response 
    >>> api_query_s = 'https://datasource.kapsarc.org/api/records/1.0/search/?dataset=solar-consumption-in-mtoe&facet=year&facet=million_tonnes_oil_equivalent&refine.year=2017'
"""


def gen_fields_fromdataset(records: list):
    for el in records:
        try:
            fields = dict(el).get('fields')
            yield fields
        except Exception as e:
            print(e.args)
            continue


def gen_records_fromdataset(api_qery: str, logger=None):
    log = logger or logger_wrapper().dummy_logger()
    http = urllib3.PoolManager()
    resp = http.request('GET', api_qery, preload_content=False)
    log.info(resp.headers)
    jsonstruct = json.load(resp)
    records: list = dict(jsonstruct).get('records')
    return records


def transform_to_csv(rows, logger: Optional[Logger] = None):
    log = logger or logger_wrapper().dummy_logger()
    part = json.dumps(rows)
    df = pandas.read_json(part)
    log.info(df)
    return df.to_csv()
