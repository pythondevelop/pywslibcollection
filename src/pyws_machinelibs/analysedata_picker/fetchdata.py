"""
Get Datas from different places of open data location from internet
"""
import logging
import os
import tarfile
from pathlib import Path
import requests
from urllib.parse import urlparse
import pandas as pd
from pandas import DataFrame

from .ml_datasource_definitions import CsvParsingInfo


class DataManager():
    @staticmethod
    def create_panda_fromcsv(descriptor: CsvParsingInfo, root_path: Path = None) -> DataFrame:
        if descriptor.is_local_file:
            path = Path(root_path, descriptor.path) if root_path else Path(descriptor.path)
            logging.info(f'data path -- {path} -- {path.cwd()}')
            print('[Workflow Info] - ', 'data path', path, ' ', path.cwd(), sep='---')
        else:
            path = descriptor.path
        return pd.read_csv(path, sep=descriptor.seperator, header=descriptor.header)

    @staticmethod
    def save_data_from_url(source_url, path=('res', 'tmp-dataset')) -> Path:
        fname = urlparse(source_url).path.split('/')[-1] if urlparse(source_url).path else "data.tgz"
        temp = Path(*path)
        temp.mkdir(parents=True, exist_ok=True)
        tgz_path = Path(temp, fname)
        response = requests.get(source_url, stream=True)
        if response.status_code != 200:
            raise ConnectionError('could not download {}\nerror code: {}'.format(source_url, response.status_code))
        if response.status_code == 200:
            with open(tgz_path, 'wb+') as f:
                f.write(response.raw.read())
        return tgz_path

    @staticmethod
    def _py_files(members):
        for tarinfo in members:
            if os.path.splitext(tarinfo.name)[1] == ".csv":
                yield tarinfo

    @staticmethod
    def create_panda_from_zipped(url, path=('res', 'tmp-dataset')):
        tgz_path = DataManager.save_data_from_url(url, path)
        tgz = tarfile.open(tgz_path)
        tgz.extractall(members=DataManager._py_files(tgz), path=tgz_path.parent)
        tgz.close()
        DataManager._py_files(tgz)
        names = [info.name for info in DataManager._py_files(tgz)]
        # now only first csv returns
        df = DataManager.create_panda_fromcsv(CsvParsingInfo(path=Path(*path, names[0]), seperator=','))
        return df
