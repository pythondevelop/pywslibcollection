import numpy as np

# TODO Start here next session
class Perceptron(object):
    """Perzeptron-Klassifizierer

        Parameter
        ---------
        eta : float
            Lernrate (zwischen 0.0 und 1.0)
        n_iter : int
            Durchläufe der Trainingsdatenmenge (Epochen)

        Attribute
        ---------
        w_ : 1d-array
            Gewichtungen nach Anpassung
        errors_ : list
            Anzahl der Fehlklassifizierungen pro Epoche

        """

    def __init__(self, eta=0.01, n_iter=10):
        self.eta = eta
        self.n_iter = n_iter

    def fit(self, matrix, y):
        """Anpassen an die Trainingsdaten

        Parameter
        ---------
        X : {array-like}, shape = [n_samples, n_features]
            Trainingvektoren, n_samples ist
            die Anzahl der Objekte und
            n_features ist die Anzahl der Merkmale
        y : array-like, shape = [n_samples]
            Zielwerte

        Rückgabewert
        ------------
        self : object
        """
        self.w_ = np.zeros(1 + matrix.shape[1])
        self.errors_ = []

        for _ in range(self.n_iter):
            errors = 0
            for xi, target in zip(matrix, y):
                update = self.eta * (target - self.predict(xi))
                self.w_[1:] += update * xi
                self.w_[0] += update
                errors += int(update != 0.0)
            self.errors_.append(errors)
        return self

    def net_input(self, matrix):
        """Nettoeingabe berechnen"""
        return np.dot(matrix, self.w_[1:]) + self.w_[0]

    def predict(self, matrix):
        """Klassenbezeichnung zurückgeben"""
        return np.where(self.net_input(matrix) >= 0.0, 1, -1)
