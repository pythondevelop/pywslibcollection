https://github.com/astanin/python-tabulate

````bash
-----  ------  -------------
Sun    696000     1.9891e+09
Earth    6371  5973.6
Moon     1737    73.5
Mars     3390   641.85
-----  ------  -------------
````

````bash
Planet      R (km)    mass (x 10^29 kg)
--------  --------  -------------------
Sun         696000           1.9891e+09
Earth         6371        5973.6
Moon          1737          73.5
Mars          3390         641.85
````

````bash
 Age  Name
-----  ------
   24  Alice
   19  Bob  

-  -  --
0  F  24
1  M  19
-  -  --
````

````bash
======  =====
item      qty
======  =====
spam       42
eggs      451
bacon       0
======  =====
````

````bash
╒════╤═══════════╤═══════════════╕
│    │ power     │ verdict       │
╞════╪═══════════╪═══════════════╡
│  0 │ -10.0 dBm │ Verdict.ERROR │
├────┼───────────┼───────────────┤
│  1 │ -20.0 dBm │ Verdict.FAIL  │
├────┼───────────┼───────────────┤
│  2 │ -30.0 dBm │ Verdict.FAIL  │
├────┼───────────┼───────────────┤
│  3 │ -40.0 dBm │ Verdict.FAIL  │
╘════╧═══════════╧═══════════════╛

````