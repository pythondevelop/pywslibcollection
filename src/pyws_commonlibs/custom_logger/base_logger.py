"""
Prototye status
Logger manager class to handle with custom Loggings
of XLAPI Trace
"""
from typing import Dict, Optional

"""
Prototye status
Logger manager class to handle with custom Loggings
e.g for xlapi script sequences
"""
import logging
import sys
from enum import Enum
import itertools

# coloring vorschläge
# https://stackoverflow.com/questions/384076/how-can-i-color-python-logging-output

DEFAULT_LOGGING_CONFIG = {
    "level": "INFO",
    "file_log_level": "DEBUG",
    "stdout_stream_handler_filter": [logging.DEBUG, logging.INFO, logging.WARNING],
    "stderr_stream_handler_filter": [logging.ERROR, logging.CRITICAL],
    "xlapi_log_handler_filter": [
        logging.DEBUG,
        logging.INFO,
        logging.WARNING,
        logging.ERROR,
        logging.CRITICAL,
    ],
}

XLAPI_DETAILED_FORMAT = "[%(levelname)8s] [%(asctime)s] [%(thread)7d] [%(module)s.%(funcName)s:%(lineno)d]: %(message)s"
XLAPI_SIMPLE_FORMAT = "[(name)s %(levelname)s] %(message)s"
SIMPLE_FORMAT = "[%(asctime)s] [%(levelname)-8s] %(name)-12s : %(message)s"
COLORFORMAT = (
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s (%(filename)s:%(lineno)d)"
)


# format = grey + COLORFORMAT + reset

# not understand how it is the usage
# https://gist.github.com/iansan5653/c4a0b9f5c30d74258c5f132084b78db9
class LoggingColorSets:
    _COLOR = {
        1: ["\x1b[38;21m", "GREY"],
        2: ["\x1b[33;21m", "YELLOW"],
        3: ["\x1b[31;21m", "RED"],
        4: ["\x1b[31;1m", "BOLD_RED"],
        5: ["\x1b[0m", "RESET"],
    }
    BaseColors = Enum(
        value="Color",
        names=itertools.chain.from_iterable(
            itertools.product(v, [k]) for k, v in _COLOR.items()
        ),
    )


# more simple but Less generic !!
class LoggingColorBasic(Enum):
    """
    print(' Basic Foreground Colors:')
    print('\t\x1b[30m Black foreground\x1b[0m')
    print('\t\x1b[31m Red foreground\x1b[0m')
    print('\t\x1b[32m Green foreground\x1b[0m')
    print('\t\x1b[33m Yellow foreground\x1b[0m')
    print('\t\x1b[34m Blue foreground\x1b[0m')
    print('\t\x1b[35m Magenta foreground\x1b[0m')
    print('\t\x1b[36m Cyan foreground\x1b[0m')
    print('\t\x1b[37m White foreground\x1b[0m')
    print('\t\x1b[39m Default foreground color \x1b[0m')
    """

    RESET = 0
    GREY = 1
    YELLOW = 2
    RED = 3
    GREEN = 4

    def __str__(self):
        _COLOR = {
            0: "\x1b[0m",
            1: "\x1b[38m",
            2: "\x1b[33m",
            3: "\x1b[31m",
            4: "\x1b[32m",
        }
        return _COLOR[self.value]


class LoggingColorUnderscoreBasic(Enum):
    """
       print(' Basic Foreground Colors:')
       print('\t\x1b[30m Black foreground\x1b[0m')
       print('\t\x1b[31m Red foreground\x1b[0m')
       print('\t\x1b[32m Green foreground\x1b[0m')
       print('\t\x1b[33m Yellow foreground\x1b[0m')
       print('\t\x1b[34m Blue foreground\x1b[0m')
       print('\t\x1b[35m Magenta foreground\x1b[0m')
       print('\t\x1b[36m Cyan foreground\x1b[0m')
       print('\t\x1b[37m White foreground\x1b[0m')
       print('\t\x1b[39m Default foreground color \x1b[0m')
       """

    RESET = 0
    GREY = 1
    YELLOW = 2
    RED = 3
    GREEN = 4

    def __str__(self):
        _COLOR = {
            0: "\x1b[0m",
            1: "\x1b[38;21m",
            2: "\x1b[33;21m",
            3: "\x1b[31;21m",
            4: "\x1b[32;21m",
        }
        return _COLOR[self.value]


class LoggerFactory:
    def __init__(self, color=LoggingColorBasic.GREY):
        self.logcolor = color
        self._register_logger = dict()

    @property
    def root_simple_logger(self):
        """
        This function does nothing if the root logger already has handlers configured.
        It is a convenience method intended for use by simple scripts to do one-shot configuration of the logging package.
        :param color:
        :type color:
        :return:
        :rtype:
        """
        logging.basicConfig(
            level=logging.INFO,
            format=str(self.logcolor) + SIMPLE_FORMAT + str(LoggingColorBasic.RESET),
            datefmt="%m-%d %H:%M",
            stream=sys.stdout,
        )
        _lg = logging.getLogger()
        return _lg

    @property
    def loggers(self):
        return self._register_logger

    def register_new_logger(self, name: Optional[str] = None) -> logging.Logger:
        if not name:
            return None
        _logger = logging.getLogger(name)
        _logger.setLevel(logging.INFO)
        _logger.addHandler(
            self.create_handler(
                logging.INFO,
                str(self.logcolor) + SIMPLE_FORMAT + str(LoggingColorBasic.RESET),
            )
        )
        self._register_logger[name] = _logger
        return _logger

    def create_handler(self, log_level: int, formatstring: str) -> logging.Handler:
        """Configure handler and return list of levels."""
        handler = logging.StreamHandler()
        #       handler.name = handler_name
        formatter = logging.Formatter(formatstring)
        handler.setFormatter(formatter)
        handler.setLevel(log_level)
        # wanted_levels = xlapi_logging_config[f"{handler_name}_filter"]
        # handler.addFilter(new_filter)
        return handler



class logger_wrapper():

    def dummy_logger(self):
        dummy = logging.getLogger('dummy')
        # dummy.setLevel(logging.CRITICAL)
        logging.disable()
        return dummy
    # Optional Logger

    @property
    def log_writer(self) -> logging.Logger:
        if self._logger:
            return self._logger
        # disabled dummy logger
        self._logger = logging.getLogger('dummy')
        self._logger.disabled = True


    def __init__(self, logger=None):
            self._logger = logger

    #def __init__(self, logger=logging.getLogger()):
    #        self._logger = logger



if __name__ == "__main__":
    report = LoggerFactory(color=LoggingColorBasic.GREEN).register_new_logger(
        name="test_logger"
    )
    report.info("simmple logger ")
    report.warning("simmple logger ")
    report.critical("simmple logger ")
    report = LoggerFactory(color=LoggingColorBasic.GREY).register_new_logger(
        name="test_logger"
    )
    report.info("simmple logger ")
    report.warning("simmple logger ")
    report.critical("simmple logger ")
    report = LoggerFactory(color=LoggingColorBasic.YELLOW).register_new_logger(
        name="test_logger_sec"
    )
    report.info("simmple logger ")
    report.warning("simmple logger ")
    report.critical("simmple logger ")
    loggers = LoggerFactory()
    report = loggers.register_new_logger(name="gods_logger")
    report.error("good is failed")
    loggers.logcolor = LoggingColorUnderscoreBasic.GREEN
    report.error("good is failed")
    report = loggers.register_new_logger(name="gods_logger")
    report.error("good is failed")
    report.error("good is failed")

    print(
        f'\n {str(LoggingColorUnderscoreBasic.YELLOW)} {30 * "+-"} {str(LoggingColorBasic.RESET)}'
    )
    print(
        str(LoggingColorBasic.YELLOW) + "das ist color" + str(LoggingColorBasic.RESET)
    )
    print(
        str(LoggingColorUnderscoreBasic.YELLOW)
        + "immer noch color?"
        + str(LoggingColorBasic.RESET)
    )

    LoggerFactory(color=LoggingColorBasic.RED).root_simple_logger.log(
        logging.WARNING, "WARNING Message"
    )
    # the coloring in this simple root logger use case only works one time at define the basic config
    # for more there more to do with stream and handler ...
    LoggerFactory(color=LoggingColorBasic.YELLOW).root_simple_logger.log(
        logging.INFO, "INFO Message"
    )
    # basic config bleibt unverändert lifcycle script run
    print(
        f'\n {str(LoggingColorUnderscoreBasic.YELLOW)} {30 * "+-"} {str(LoggingColorBasic.RESET)}'
    )
