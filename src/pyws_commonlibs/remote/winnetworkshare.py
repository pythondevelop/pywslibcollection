import os
from typing import Tuple

import pywintypes
import win32wnet


class InstrumentNetworkShareWindows():
    SHARE_PWD = "instrument"
    CONNECT_INTERACTIVE = 0x00000008

    def connect_testsystemdrive(self, host_name, share_name=r'c\ProgramData\Rohde-Schwarz'):
        SHARE_USER = r'\instrument'
        SHARE_FULL_NAME = InstrumentNetworkShareWindows.build_sharefullname(host_name, share_name)

        pwd = host_name.split('-')[1]

        net_resource = win32wnet.NETRESOURCE()
        net_resource.lpRemoteName = SHARE_FULL_NAME
        flags = 0
        # flags |= CONNECT_INTERACTIVE
        print("Trying to create connection to: {:s}".format(SHARE_FULL_NAME))
        try:
            win32wnet.WNetAddConnection2(net_resource, pwd, SHARE_USER, flags)
        except pywintypes.errors as e:
            print(e)
        else:
            print("Success!")
    @staticmethod
    def list_files(host_name='localhost', share_name=r'c:\ProgramData\Rohde-Schwarz') ->Tuple[str, str, str, str]:
        share = InstrumentNetworkShareWindows.build_sharefullname(host_name, share_name)
        for currentpath, folders, files in os.walk(share):
            print(f"{currentpath}, {folders}, {files}")
            yield currentpath, folders, files

    @staticmethod
    def build_sharefullname(host_name='localhost', share_name=r'c:\ProgramData\Rohde-Schwarz'):

        if host_name.lower() == 'localhost':
            return share_name
        return os.path.sep * 2 + os.path.sep.join((host_name, share_name))
