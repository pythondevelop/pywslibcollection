import functools
import logging
from dataclasses import dataclass, field
from typing import ClassVar, Any

import time

"""
https://stackoverflow.com/questions/7370801/how-to-measure-elapsed-time-in-python
DEF https://realpython.com/python-timer/
STATUS : In Dev
    >>> watch.start()
    >>>     for i in range(1,10000000):
    >>>         math.sin(math.log10(i))
    >>>     watch.stop()

"""


class TimerError(Exception):
    """A custom exception used to report errors in use of Timer class"""


@dataclass
class Stopwatch:
    timers: ClassVar = dict()
    name: Any = None
    text: Any = f"Elapsed time: {{:0.4f}} seconds"
    logger: logging.Logger = None
    _start_time: Any = field(default=None, init=False, repr=False)
    _time_at_stop: Any = field(default=None, init=False, repr=False)

    @property
    def elapsed_Time(self):
        if self.stoptime is not None:
            return self.stoptime
        if self._start_time is None:
            raise TimerError(f"Timer is not running. Use .start() to start it")
        return time.perf_counter() - self._start_time

    @property
    def stoptime(self):
        return self._time_at_stop

    @property
    def log_writer(self) -> logging.Logger:
        if not self.logger:
            self.logger = logging.getLogger('Stopwatch')
            self.logger.disable = True
        # oder dummy logger der nichts macht oder logging.disable()
        return self.logger

    def __post_init__(self):
        if self.name:
            self.timers.setdefault(self.name, 0)

    def start(self):
        """Start a new timer"""
        if self._start_time is not None:
            raise TimerError(f"Timer is running. Use .stop() to stop it")
        self._start_time = time.perf_counter()

    def zwischenzeit(self):
        self.log_writer.info(self.text.format(self.elapsed_Time))
        return self.elapsed_Time

    def stop(self):
        """Stop the timer, and report the elapsed time"""
        if self._start_time is None:
            raise TimerError(f"Timer is not running. Use .start() to start it")
        self.log_writer.info(self.text.format(self.elapsed_Time))
        self._time_at_stop = self.elapsed_Time
        self._start_time = None
        if self.name:
            self.timers[self.name] += self.elapsed_Time

    def __call__(self, func):
        # support of decorator - meta date
        @functools.wraps(func)
        def decorator_wrapper(*args, **kwargs):
            with self:
                return func(*args, **kwargs)

        return decorator_wrapper

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, exc_type, exc_value, exc_tb):
        self.stop()

    def __del__(self):
        logging.disable(logging.NOTSET)

    @classmethod
    def createandstart(cls, text=f"Elapsed time: {{:0.4f}} seconds", logger=None):
        w = cls(text=text, logger=logger)
        w.start()
        return w


def measuretime(func):
    @functools.wraps(func)
    def wrapper_measuretime(*args, **kwargs):
        # Do something before
        start = time.perf_counter()

        value = func(*args, **kwargs)
        stop = time.perf_counter()
        elapsed_time = stop - start

        logging.info(f"Elapsed time: {elapsed_time:0.4f} seconds")
        # Do something after
        return value

    return wrapper_measuretime


# TODO IDEE
"""@attention

# context manager for this polling loops 
# with stopwatch()

watch = Stopwatch()
watch.start()
while(True):
    try:
        logging.info(f' --> {(scpi(inst).query_str("*IDN?"))}')
        if watch.zwischenzeit() > 10.0
            
            break
    except:
        pass



@contextlib.contextmanager
def stopwatch(what: str):
    """'Context manager for recording the execution time of a block of code.'"""
    start_time = time.monotonic()
    try:
        yield
    finally:
        end_time = time.monotonic()
        logging.debug("%s returned after %0.1f seconds.", what, end_time - start_time)



"""
