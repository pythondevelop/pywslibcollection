
def yes_no(answer) -> bool:
    yes = {"yes", "y", "ye", ""}
    no = {"no", "n"}

    while True:
        choice = input(answer).lower()
        if choice in yes:
            return True
        elif choice in no:
            return False
        else:
            print("Please respond with 'yes' or 'no'\n ")